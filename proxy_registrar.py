#python proxy_registrar.py pr.xml

import socket
import sys
import socketserver
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import json
import time


class FileHandler(ContentHandler): #adaptada de Uaclient

    def __init__(self):

        # Definición de la lista de atributos
        self.data_xml = []

        self.server = ['name', 'ip', 'puerto']
        self.database = ['path', 'passwdpath']
        self.log = ['path']

        self.tags = {"server": self.server,
                     "database": self.database,
                     "log": self.log}

    def startElement(self, name, attrs):

        dicctag = {}

        if name in self.tags:
            dicctag = {'Tag': name}
            for atribute in self.tags[name]:
                dicctag[atribute] = attrs.get(atribute, "")
            self.data_xml.append(dicctag)

    def get_tags(self):

        return self.data_xml


#Clase sacada de la Practica 6
class ProxyHandler(socketserver.DatagramRequestHandler):
    """
    RPT server class
    """
    dicc_user = {} #diccionario con los usuarios registrados

    #Sacados de la P4
    def registered2json(self, jsonfile):

        file = open(jsonfile, 'w')
        json.dump(self.dicc_user, file)

    def json2registered(self, jsonfile):
        try:
            file = open(jsonfile, "r")
            data = json.load(file)
            self.dicc_user = data
        except FileNotFoundError:
            pass

    def handle(self):

        tiempo = time.time()
        current_time = time.strftime('%Y%m%d%H%M%S', time.localtime(tiempo))
        #diccionario con las respuestas
        diccmetods = {'INVITE': "SIP/2.0 100 Trying\r\n\r\n" + "SIP/2.0 180 Ringing\r\n\r\n" + "SIP/2.0 200 OK\r\n\r\n" + 'Content-Type: application/sdp\r\n\r\n',
                      'ACK': 'ACK',
                      'BYE': 'Sent to ' + server_ip + ':' + str(server_port) + ': ' + "SIP/2.0 200 OK\r\n\r\n",
                      'REGISTER': 'REGISTER'}

        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            data_rcv = self.rfile.read().decode('utf-8')
            data_tosend = data_rcv

            # Si no hay más líneas salimos del bucle infinito
            if not data_rcv:
                break
            #Sacar el metodo
            data_rcv = data_rcv.split()
            print(data_rcv)
            metod = data_rcv[0]
            user = data_rcv[1].split(":")[1]

            if metod in diccmetods:
                if metod == "REGISTER": #REGISTER sip:user1@server1:4001 SIP/2.0 Expires: 3600
                    port = data_rcv[1].split(":")[2]
                    exp_time = data_rcv[4]
                    if len(data_rcv) == 5: #Registro sin contraseña
                        print("User " + user + " not authorized")
                        self.wfile.write(b"SIP/2.0 401 Unauthorized" + b"\r\n" + b"WWW Authenticate: Digest nonce=" + b"898989898798989898989 \r\n")
                        msg_tolog = "Sent to " + server_ip + ':' + str(port) + ':'  + "SIP/2.0 401 Unauthorized" + '\r\n'
                        log_writer(msg_tolog)
                    elif len(data_rcv) == 8: # REGISTER sip:leonard@bigbang.org:1234 SIP/2.0 Expires: 3600 Authorization: Digest response="123123212312321212123"
                        user_data = [server_ip, exp_time, port, current_time]
                        self.dicc_user[user] = user_data
                        self.registered2json(database_path)
                        self.wfile.write(b"SIP/2.0 200 OK" + b"\r\n\r\n") #se envia ok de vuleta al cliente
                        msg_tolog = "Sent to " + server_ip + ':' + str(port) + ':' + "SIP/2.0 200 OK" + '\r\n'
                        log_writer(msg_tolog)
                        print("User " + user + " authorized")

                else: #ACK, INVITE o BYE
                    if user not in self.dicc_user:
                        self.wfile.write(b"SIP/2.0 404 User Not Found" + b"\r\n\r\n")
                        print("No invite, user not found")
                    else:
                        port = self.dicc_user[user][2]
                        my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                        my_socket.connect((server_ip, int(port)))
                        my_socket.send(bytes(data_tosend, 'utf-8') + b'\r\n')
                        print("TRAZA 1 DE INVITE" + data_tosend)
                        data = my_socket.recv(1024) #Respuesta del servidor que recibe el invite
                        print("TRAZA DE INVITE" + str(data))
                        self.wfile.write(data)

            else:
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
                msg_tolog = 'Error: SIP/2.0 400 Bad Request.'
                log_writer(msg_tolog)






def log_writer(mensaje): #igual que en uaclient
    log = open(file_log, "a")  # para escribir por el final
    tiempo = time.time()
    current_time = time.strftime('%Y%m%d%H%M%S', time.localtime(tiempo))
    log.write(current_time + " " + mensaje.replace("\r\n", " ") + "\r\n")
    log.close()





if __name__ == "__main__":

    try:
        config = sys.argv[1]
    except:
        sys.exit("Usage: python3 proxy_registrar.py config")

    parser = make_parser()
    XmlHandler = FileHandler()
    parser.setContentHandler(XmlHandler)
    parser.parse(open(config))
    list_xml = XmlHandler.get_tags()

        # Asignar datos del fichero a variables (copiado de uaclient y adaptado)

    server_name = list_xml[0]['name']
    server_ip = list_xml[0]['ip']
    server_port = list_xml[0]['puerto']
    database_path = list_xml[1]['path']
    database_pssws = list_xml[1]['passwdpath']
    file_log = list_xml[2]['path']

    serv = socketserver.UDPServer((server_ip, int(server_port)), ProxyHandler)

    print("Server " + server_name + " listening at port " + server_port + "...")
    serv.serve_forever()
