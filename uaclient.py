"""
Programa cliente que abre un socket a un servidor
"""
# python3 uaclient.py config metodo opcion
# python3 uaclient.py ua1.xml REGISTER 3600
# python3 uaclient.py ua1.xml INVITE alguien@algo
# python3 uaclient.py ua1.xml BYE alguien@algo
# python3 uaclient.py ua1.xml REGISTER 0

"""User Agent Client."""




import sys
import socket
import hashlib
import os
import time
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
class FileHandler(ContentHandler):

    def __init__(self):

        # Definición de la lista de atributos
        self.data_xml = []

        self.account = ['username', 'passwd']
        self.uaserver = ['ip', 'puerto']
        self.rtpaudio = ['puerto']
        self.regproxy = ['ip', 'puerto']
        self.log = ['path']
        self.audio = ['path']

        self.tags = {"account": self.account,
                     "uaserver": self.uaserver,
                     "rtpaudio": self.rtpaudio,
                     "regproxy": self.regproxy,
                     "log": self.log,
                     "audio": self.audio}

    def startElement(self, name, attrs):

        dicctag = {}

        if name in self.tags:
            dicctag = {'Tag': name}
            for atribute in self.tags[name]:
                dicctag[atribute] = attrs.get(atribute, "")
            self.data_xml.append(dicctag)

    def get_tags(self):

        return self.data_xml


def log_writer(mensaje):
    log = open(file_log, "a")  # para escribir por el final
    tiempo = time.time()
    current_time = time.strftime('%Y%m%d%H%M%S', time.localtime(tiempo))
    log.write(current_time + " " + mensaje.replace("\r\n", " ") + "\r\n")
    log.close()


if __name__ == "__main__":

    try:
        config = sys.argv[1]
        metod = sys.argv[2]
        option = sys.argv[3]
    except:
        sys.exit("Usage: python3 uaclient.py config metodo opcion")

    # RECEIVER = ID[0:ID.find("@")]
    # IP = ID[ID.find("@")+1:ID.find(":")]
    # SIPport = int(ID[ID.find(":")+1:])
    # MSGtail = " sip:" + RECEIVER + "@IP SIP/2.0"

    # parse del fichero
    parser = make_parser()
    XmlHandler = FileHandler()
    parser.setContentHandler(XmlHandler)
    parser.parse(open(config))
    list_xml = XmlHandler.get_tags()

    # Asignar datos del fichero a variables

    username = list_xml[0]['username']
    passwd = list_xml[0]['passwd']
    server_ip = list_xml[1]['ip']
    # print(username)
    server_port = list_xml[1]['puerto']
    audio_port = list_xml[2]['puerto']
    proxy_ip = list_xml[3]['ip']
    proxy_port = list_xml[3]['puerto']
    file_log = list_xml[4]['path']
    file_audio = list_xml[5]['path']

    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    #my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((proxy_ip, int(proxy_port)))

    diccmetods = {'INVITE': 'INVITE' + " sip:" + option + " SIP/2.0 \r\n\r\n" + "Content-Type: application/sdp \r\n" + "v=0 \r\n" + "o=" + username + " " + server_ip + "\r\n" + "s=misesion \r\n" + "t=0 \r\n" + "m=audio " + audio_port + " RTP \r\n\r\n",
                  'ACK': 'ACK' + " sip:" + option + " SIP/2.0 \r\n",
                  'REGISTER': 'REGISTER' + " sip:" + username + ":" + server_port + " SIP/2.0" + "\r\n" + "Expires: " + option + "\r\n",
                  'BYE': 'BYE' + " sip:" + option + " SIP/2.0 \r\n\r\n"}

    if metod in diccmetods:

        if metod == "REGISTER":
            line = diccmetods[metod]
            print("Sending: ", line)
            my_socket.send(bytes(line, 'utf-8') + b'\r\n')
            # log
            msg_tolog = " Sent to " + proxy_ip + ':' + str(proxy_port) + ':' + line
            log_writer(msg_tolog)
            # Para recibir
            data_rcv = my_socket.recv(1024)
            data401 = data_rcv
            print('Received-- ', data_rcv.decode('utf-8'))
            data_rcv = data_rcv.decode('utf-8').split()
            if data_rcv[1] == "401":
                print('Received from server:', data401)
                msg_tolog = "Received " + str(proxy_ip) + ':' + str(proxy_port) + ':' + str(data401)
                log_writer(msg_tolog)
                line = diccmetods[metod] + "Authorization: Digest response=" + "123123212312321212123 \r\n"
                print("Sending: ", line)
                my_socket.send(bytes(line, 'utf-8') + b'\r\n')
                msg_tolog = " Sent to " + proxy_ip + ':' + str(proxy_port) + ':' + line
                log_writer(msg_tolog)
                # Para recibir
                data_rcv = my_socket.recv(1024)
                print('Received-- ', data_rcv.decode('utf-8'))
            #    data_rcv = data_rcv.decode('utf-8').split()

        elif metod == "INVITE":
            line = diccmetods[metod]
            print("Sending: ", line)
            my_socket.send(bytes(line, 'utf-8') + b'\r\n')
            # log
            msg_tolog = " Sent to " + proxy_ip + ':' + str(proxy_port) + ':' + line
            log_writer(msg_tolog)
            ######
            #prueba = line.split()
            #print("PRUEBA:   ")
            #print(prueba[7])
            #print(prueba[11])
            ######
            data_rcv = my_socket.recv(1024)
            data_404 = data_rcv.decode('utf-8')
            print('Received-- ', data_rcv.decode('utf-8'))
            data_rcv = data_rcv.decode('utf-8').split()

            if data_rcv[1] == '404':
                print('Received from server:', data_404)
            elif data_rcv[1] == '100':
                line = diccmetods["ACK"]
                print("Sending: ", line)
                my_socket.send(bytes(line, 'utf-8') + b'\r\n')
                # log
                msg_tolog = " Sent to " + proxy_ip + ':' + str(proxy_port) + ':' + line
                log_writer(msg_tolog)
                # Envío RTP
                mp32rtp = './mp32rtp -i ' + server_ip + ' -p ' + str(audio_port) + ' < ' + str(file_audio)
                print("Executing... ", mp32rtp)
                os.system(mp32rtp)
                msg_tolog = 'Sent audio to ' + server_ip + ':' + str(audio_port)
                log_writer(msg_tolog)

        elif metod == "BYE":
            line = diccmetods[metod]
            print("Sending: ", line)
            my_socket.send(bytes(line, 'utf-8') + b'\r\n')
            # log
            msg_tolog = " Sent to " + proxy_ip + ':' + str(proxy_port) + ':' + line
            log_writer(msg_tolog)
            data_rcv = my_socket.recv(1024)
            data200 = data_rcv
            print('Received-- ', data_rcv.decode('utf-8'))
            data_rcv = data_rcv.decode('utf-8').split()

            if data_rcv[1] == '200':
                print('Received from server:', data200)
                msg_tolog = "Received " + proxy_ip + ':' + str(proxy_port) + ':' + data200
                log_writer(msg_tolog)

                # Cerrar
                print("Finishing service...")
                my_socket.close()
                print("Service ended")
