"""
Programa cliente que abre un socket a un servidor
"""
# python3 uaserver.py ua1.xml

"""User Agent Client."""


import sys
import socketserver
import os
import time
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from uaclient import FileHandler

#Clase sacada de la Practica 6
class RTPHandler(socketserver.DatagramRequestHandler):
    """
    RPT server class
    """

    def handle(self):

        #diccionario con las respuestas
        diccmetods = {'INVITE': "SIP/2.0 100 Trying\r\n\r\n" + "SIP/2.0 180 Ringing\r\n\r\n" + "SIP/2.0 200 OK\r\n\r\n" + 'Content-Type: application/sdp\r\n\r\n' + "v=0\r\n" + "o="  + str(username) + " " + server_ip + "\r\n" + "s=MySession\r\n" + "t=0\r\n" + "m=audio" + audio_port + " RTP\r\n",
                      'ACK': 'ACK',
                      'BYE': 'Sent to ' + proxy_ip + ':' + str(proxy_port) + ': ' + "SIP/2.0 200 OK\r\n\r\n"}

        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            data_rcv = self.rfile.read().decode('utf-8')

            # Si no hay más líneas salimos del bucle infinito
            if not data_rcv:
                break
            #Sacar el metodo
            data_toprint = data_rcv
            data_rcv = data_rcv.split()
            metod = data_rcv[0]


            if metod == 'INVITE': #respuesta con 100 180 200 + SDP
                print("Received: " + data_toprint)
                #log de recepcion
                msg_tolog = "Received " + proxy_ip + ':' + str(proxy_port) + ':' + data_toprint
                log_writer(msg_tolog)
                #construccion mensaje
                reply_invite = diccmetods[metod]
                print("Traza de audioport   ")
                print(audio_port)
                print(reply_invite)
            #reply_invite =
                self.wfile.write(bytes(reply_invite, 'utf-8') + b'\r\n')
                #log de envio
                msg_tolog = 'Sent to ' + proxy_ip + ':' + str(proxy_port) + ': ' + diccmetods[metod]
                log_writer(msg_tolog)

            elif metod == 'ACK': #Iniciar envio de rtp
                print("Received: " + data_toprint)
                #log de recepcion
                msg_tolog = "Received " + proxy_ip + ':' + str(proxy_port) + ':' + data_toprint
                log_writer(msg_tolog)
                #Envio de RTP
                mp32rtp = "./mp32rtp -i " + server_ip + " -p " + str(audio_port) + " < " + str(file_audio)
                print("Executing... ", mp32rtp)
                os.system(mp32rtp)
                msg_tolog = 'Sent audio to ' + server_ip + ':' + str(audio_port)
                log_writer(msg_tolog)

            elif metod == 'BYE':
                print("Received: " + data_toprint)
                #log de recepcion
                msg_tolog = "Received " + proxy_ip + ':' + str(proxy_port) + ':' + data_toprint
                log_writer(msg_tolog)
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                msg_tolog = diccmetods[metod]
                log_writer(msg_tolog)

            elif metod not in diccmetods:
                self.wfile.write(b"SIP/2.0 405 metod Not Allowed")
                msg_tolog = 'Error: SIP/2.0 405 metod Not Allowed.'
                log_writer(msg_tolog)
            else:
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
                msg_tolog = 'Error: SIP/2.0 400 Bad Request.'
                log_writer(msg_tolog)

def log_writer(mensaje):
    log = open(file_log, "a")  # para escribir por el final
    tiempo = time.time()
    current_time = time.strftime('%Y%m%d%H%M%S', time.localtime(tiempo))
    log.write(current_time + " " + mensaje.replace("\r\n", " ") + "\r\n")
    log.close()


if __name__ == "__main__":

    try:
        config = sys.argv[1]
    except:
        sys.exit("Usage: python3 uaserver.py config")

    parser = make_parser()
    XmlHandler = FileHandler()
    parser.setContentHandler(XmlHandler)
    parser.parse(open(config))
    list_xml = XmlHandler.get_tags()

    # Asignar datos del fichero a variables (copiado de uaclient)

    username = list_xml[0]['username']
    passwd = list_xml[0]['passwd']
    server_ip = list_xml[1]['ip']
    print(username)
    server_port = list_xml[1]['puerto']
    audio_port = list_xml[2]['puerto']
    proxy_ip = list_xml[3]['ip']
    proxy_port = list_xml[3]['puerto']
    file_log = list_xml[4]['path']
    file_audio = list_xml[5]['path']
    print(file_audio)

    serv = socketserver.UDPServer((server_ip, int(server_port)), RTPHandler)
    print("Listening \r\n")
    serv.serve_forever()
